## Name
Library Management System Using Django

## Description 
This is a simple e-library system built with django, python, html, CSS and bootstrap.Different kinds of books are posted by the librarians through their respective accounts and those interested can view and borrow them.Failure to return the books on the required date will incur a penalty upon the borrower.
The user will be notified a day before the book's due date reminding them to return the borrowed book. A book which is already borrowed is unavailable for other users.

## FEATURES OF THE PPROJECT
A student can:
        signup/login 
        view borrowed books
        view available books
        view their fines

A librarian or admin can:
        signup/login
        add books to the library
        view available books
        issue a book 
        view borrowed books
        view students
        view fines of students

<!-- 
## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers. -->
